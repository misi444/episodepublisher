﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace EpisodePublisher
{
    public static class Config
    {
        public static string Body => ConfigurationManager.AppSettings["Body"];

        public static string DownloadLocation => ConfigurationManager.AppSettings["DownloadLocation"];

        public static string FeedUrl => ConfigurationManager.AppSettings["FeedUrl"];

        public static string From => ConfigurationManager.AppSettings["From"];

        public static string GmailPassword => ConfigurationManager.AppSettings["GmailPassword"];

        public static string GmailUsername => ConfigurationManager.AppSettings["GmailUsername"];

        public static string PublishLocation => ConfigurationManager.AppSettings["PublishLocation"];

        public static List<MailAddress> Recipients => ConfigurationManager.AppSettings["Recipients"].Split(';').Select(x => new MailAddress(x)).ToList();

        public static List<MailAddress> Developers => ConfigurationManager.AppSettings["Developers"].Split(';').Select(x => new MailAddress(x)).ToList();

        public static string Subject => ConfigurationManager.AppSettings["Subject"];

        public static string ErrorSubject => ConfigurationManager.AppSettings["ErrorSubject"];

        public static string SerieTitle => ConfigurationManager.AppSettings["SerieTitle"];

        public static string Season => ConfigurationManager.AppSettings["Season"];

        public static string Episode => ConfigurationManager.AppSettings["Episode"];

        public static string Language => ConfigurationManager.AppSettings["Language"];

        public static List<string> Languages => ConfigurationManager.AppSettings["Language"].Split(',').ToList();

        public static string PublishUrl => ConfigurationManager.AppSettings["PublishUrl"];

        public static string FfmpegCommand => ConfigurationManager.AppSettings["FfmpegCommand"];

        public static int TorrentPort => int.Parse(ConfigurationManager.AppSettings["TorrentPort"]);

        public static bool Windows => bool.Parse(ConfigurationManager.AppSettings["Windows"]);

        public static bool UseRss => bool.Parse(ConfigurationManager.AppSettings["UseRss"]);

        public static bool DownloadTorrent => bool.Parse(ConfigurationManager.AppSettings["DownloadTorrent"]);

        public static bool ExtractContent => bool.Parse(ConfigurationManager.AppSettings["ExtractContent"]);

        public static bool DownloadSubtitle => bool.Parse(ConfigurationManager.AppSettings["DownloadSubtitle"]);

        public static bool BurnSubtitle => bool.Parse(ConfigurationManager.AppSettings["BurnSubtitle"]);

        public static bool BurnSubtitleLocally => bool.Parse(ConfigurationManager.AppSettings["BurnSubtitleLocally"]);

        public static bool CleanDownloadLocation => bool.Parse(ConfigurationManager.AppSettings["CleanDownloadLocation"]);

        public static bool SendNotification => bool.Parse(ConfigurationManager.AppSettings["SendNotification"]);

        public static string WolMac => ConfigurationManager.AppSettings["WolMac"];

        public static int WolPort => int.Parse(ConfigurationManager.AppSettings["WolPort"]);

        public static IPAddress TcpIp => IPAddress.Parse(ConfigurationManager.AppSettings["TcpIp"]);

        public static IPAddress TcpMask => IPAddress.Parse(ConfigurationManager.AppSettings["TcpMask"]);

        public static int TcpPort => int.Parse(ConfigurationManager.AppSettings["TcpPort"]);
    }
}