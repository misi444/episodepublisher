﻿namespace EpisodePublisher.Services.TcpUdp
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;

    public class TcpUdpService : ServiceBase, ITcpUdpService
    {
        public TcpUdpService() : base()
        {

        }

        public string SendTcpMessage(IPAddress ip, int port, string message)
        {
            using (TcpClient client = new TcpClient(ip.ToString(), port))
            {
                byte[] data = Encoding.UTF8.GetBytes(message);
                using (NetworkStream stream = client.GetStream())
                {
                    stream.Write(data, 0, data.Length);
                    stream.Flush();

                    string responseMessage;
                    byte[] bytes = new byte[1024];
                    int i;
                    i = stream.Read(bytes, 0, bytes.Length);
                    responseMessage = Encoding.ASCII.GetString(bytes, 0, i);
                    return responseMessage;
                }
            }
        }

        public void WakeComputerUp(string macAddress, IPAddress ip, IPAddress mask, int port)
        {
            byte[] datagram = new byte[102];

            for (int i = 0; i <= 5; i++)
            {
                datagram[i] = 0xff;
            }

            string[] macDigits = macAddress.Split('-');

            int start = 6;
            for (int i = 0; i < 16; i++)
            {
                for (int x = 0; x < 6; x++)
                {
                    datagram[start + i * 6 + x] = (byte)Convert.ToInt32(macDigits[x], 16);
                }
            }
            
            IPAddress broadcastAddress = this.GetBroadcastAddress(ip, mask);

            using (UdpClient client = new UdpClient())
            {
                client.Send(datagram, datagram.Length, broadcastAddress.ToString(), port);
            }
        }

        private IPAddress GetBroadcastAddress(IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
            {
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");
            }

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }

            return new IPAddress(broadcastAddress);
        }
    }
}