﻿namespace EpisodePublisher.Services.TcpUdp
{
    using System.Net;

    public interface ITcpUdpService
    {
        void WakeComputerUp(string macAddress, IPAddress ip, IPAddress mask, int port);
        string SendTcpMessage(IPAddress ip, int port, string message);
    }
}