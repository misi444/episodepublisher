﻿namespace EpisodePublisher.Services.Email
{
    using System;

    public interface IEmailService
    {
        void SendMailNotification(string body, string title, string language);
        void SendErrorMailNotification(Exception e);
    }
}