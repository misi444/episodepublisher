﻿namespace EpisodePublisher.Services.Email
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;

    public class EmailService : ServiceBase, IEmailService
    {
        private SmtpClient smtpClient;
        private string subject;
        private string errorSubject;
        private List<MailAddress> recipients;
        private List<MailAddress> developers;

        public EmailService(string username, string password, string subject, string errorSubject, List<MailAddress> recipients, List<MailAddress> developers) : base()
        {
            this.smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(username, password)
            };

            this.subject = subject;
            this.errorSubject = errorSubject;
            this.recipients = recipients;
            this.developers = developers;
        }

        public void SendErrorMailNotification(Exception e)
        {
            using (MailMessage message = this.GetMailMessage(this.subject, e.ToString(), this.developers))
            {
                this.smtpClient.Send(message);
            }
        }

        public void SendMailNotification(string body, string title, string language)
        {
            using (MailMessage message = GetMailMessage(string.Format(this.subject, title, language), body, this.recipients))
            {
                this.smtpClient.Send(message);
            }
        }

        private MailMessage GetMailMessage(string subject, string body, List<MailAddress> recipients)
        {
            MailMessage result = new MailMessage { From = new MailAddress(Config.From), Subject = subject, Body = body, IsBodyHtml = true };
            recipients.ForEach(result.To.Add);
            return result;
        }
    }
}