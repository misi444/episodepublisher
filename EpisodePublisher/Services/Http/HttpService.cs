﻿namespace EpisodePublisher.Services.Http
{
    using System;
    using System.IO;
    using System.Net.Http;

    public class HttpService : ServiceBase, IHttpService
    {
        public HttpService() : base()
        {

        }

        public void DownloadFile(Uri uri, string downloadLocation)
        {
            using (HttpClient client = new HttpClient())
            {
                Log.InfoFormat("Downloading torrent file...");
                byte[] torrentContent = client.GetByteArrayAsync(uri.AbsoluteUri).Result;
                File.WriteAllBytes(downloadLocation, torrentContent);
            }
        }
    }
}