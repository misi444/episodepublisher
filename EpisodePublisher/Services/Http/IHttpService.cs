﻿namespace EpisodePublisher.Services.Http
{
    using System;

    public interface IHttpService
    {
        void DownloadFile(Uri uri, string downloadLocation);
    }
}