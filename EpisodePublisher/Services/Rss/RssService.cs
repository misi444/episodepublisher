﻿namespace EpisodePublisher.Services.Rss
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel.Syndication;
    using System.Xml;

    public class RssService : ServiceBase, IRssService
    {
        private string feedUrl;

        public RssService(string feedUrl) : base()
        {
            this.feedUrl = feedUrl;
        }

        public IList<Uri> GetMatchingRssUris(string filter)
        {
            Log.InfoFormat("Loading RSS feed...");
            SyndicationFeed feed;
            using (XmlReader reader = XmlReader.Create(this.feedUrl))
            {
                feed = SyndicationFeed.Load(reader);
            }

            Log.InfoFormat("RSS feed loaded. Searching for filter '{0}'...", filter);
            IList<Uri> matches = feed.Items.Where(x => x.Title.Text.ToLower().Contains(filter)).Select(x => x.Links.First().Uri).ToList();
            Log.InfoFormat("Found {0} match(es).", matches.Count);
            return matches;
        }

        public Uri GetFirstMatchingRssUri(string filter)
        {
            return this.GetMatchingRssUris(filter).First();
        }

        public Uri GetSingleMatchingRssUri(string filter)
        {
            return this.GetMatchingRssUris(filter).Single();
        }
    }
}