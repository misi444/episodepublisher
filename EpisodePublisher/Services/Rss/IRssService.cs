﻿namespace EpisodePublisher.Services.Rss
{
    using System;
    using System.Collections.Generic;

    public interface IRssService
    {
        IList<Uri> GetMatchingRssUris(string filter);

        Uri GetFirstMatchingRssUri(string filter);

        Uri GetSingleMatchingRssUri(string filter);
    }
}