﻿namespace EpisodePublisher.Services.BitTorrent
{
    using MonoTorrent.Client;
    using MonoTorrent.Common;
    using System.Threading;

    public sealed class TorrentService : ServiceBase, ITorrentService
    {
        private string downloadLocation;
        private int port;
        private bool torrentDownloadComplete;
        private ClientEngine torrentClient;

        public TorrentService(string downloadLocation, int port) : base()
        {
            this.downloadLocation = downloadLocation;
            this.port = port;

            EngineSettings settings = new EngineSettings(this.downloadLocation, this.port);
            this.torrentClient = new ClientEngine(settings);
        }

        public void DownloadTorrent(string torrentFilePath)
        {
            Torrent torrent;
            TorrentManager manager;
            this.torrentDownloadComplete = false;
            TorrentSettings torrentSettings = new TorrentSettings();
            torrent = Torrent.Load(torrentFilePath);
            manager = new TorrentManager(torrent, Config.DownloadLocation, torrentSettings);
            manager.TorrentStateChanged += Manager_TorrentStateChanged;
            torrentClient.Register(manager);
            torrentClient.StartAll();

            while (!this.torrentDownloadComplete)
            {
                Thread.Sleep(1000);
            }
        }

        public void Dispose()
        {
            this.torrentClient.Dispose();
        }

        private void Manager_TorrentStateChanged(object sender, TorrentStateChangedEventArgs e)
        {
            if (e.NewState == TorrentState.Seeding && e.TorrentManager.Complete)
            {
                this.torrentDownloadComplete = true;
            }
        }
    }
}