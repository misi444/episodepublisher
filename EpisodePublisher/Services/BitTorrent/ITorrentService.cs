﻿namespace EpisodePublisher.Services.BitTorrent
{
    using System;

    public interface ITorrentService : IDisposable
    {
        void DownloadTorrent(string torrentFilePath);
    }
}