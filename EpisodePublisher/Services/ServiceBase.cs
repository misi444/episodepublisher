﻿namespace EpisodePublisher.Services
{
    using log4net;
    using log4net.Config;

    public class ServiceBase
    {
        protected static ILog Log;

        public ServiceBase()
        {
            Log = LogManager.GetLogger(this.GetType());
            XmlConfigurator.Configure();
        }
    }
}