﻿namespace EpisodePublisher.Services.DirectoryCleaner
{
    public interface IDirectoryCleanerService
    {
        void CleanDirectory(string path);
    }
}