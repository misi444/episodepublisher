﻿namespace EpisodePublisher.Services.DirectoryCleaner
{
    using System.IO;

    public class DirectoryCleanerService : ServiceBase, IDirectoryCleanerService
    {
        public DirectoryCleanerService() : base()
        {

        }

        public void CleanDirectory(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            foreach (FileInfo file in directory.GetFiles())
            {
                file.Delete();
            }

            foreach (DirectoryInfo subdir in directory.GetDirectories())
            {
                subdir.Delete(true);
            }
        }
    }
}