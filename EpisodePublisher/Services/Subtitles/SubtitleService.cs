﻿namespace EpisodePublisher.Services.Subtitles
{
    using System.Collections.Generic;
    using OSDBnet;
    using System.Text;
    using System.IO;
    public sealed class SubtitleService : ServiceBase, ISubtitleService
    {
        private string language;
        private IAnonymousClient osdbClient;

        public SubtitleService(string language) : base()
        {
            this.language = language;
            this.osdbClient = Osdb.Login("My Application v0.1");
        }

        public void DownloadSubtitle(Subtitle subtitle, string path)
        {
            this.osdbClient.DownloadSubtitleToPath(path, subtitle);
        }

        public IList<Subtitle> SearchSubtitlesForVideoFile(string filePath)
        {
            IList<Subtitle> subtitles = this.osdbClient.SearchSubtitlesFromFile(this.language, filePath);
            return subtitles;
        }
        
        public void EnsureSubtitleIsUTF8(string subtitlePath)
        {
            Encoding currentEncoding = GetEncoding(subtitlePath);
            if (currentEncoding != Encoding.UTF8)
            {
                Log.InfoFormat("Subtitle is not in the correct encoding. Re-encoding to UTF-8...");
                string utf8string = File.ReadAllText(subtitlePath, Encoding.Default);
                File.WriteAllText(subtitlePath, utf8string);
                Log.InfoFormat("Subtitle successfully re-encoded.");
            }
        }

        public void Dispose()
        {
            this.osdbClient.Dispose();
        }

        private Encoding GetEncoding(string filename)
        {
            byte[] bom = new byte[4];
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                stream.Read(bom, 0, 4);
            }

            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76)
            {
                return Encoding.UTF7;
            }

            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
            {
                return Encoding.UTF8;
            }

            if (bom[0] == 0xff && bom[1] == 0xfe)
            {
                return Encoding.Unicode;
            }

            if (bom[0] == 0xfe && bom[1] == 0xff)
            {
                return Encoding.BigEndianUnicode;
            }

            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff)
            {
                return Encoding.UTF32;
            }

            return Encoding.ASCII;
        }
    }
}