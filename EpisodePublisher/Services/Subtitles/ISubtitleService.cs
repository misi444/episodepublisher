﻿namespace EpisodePublisher.Services.Subtitles
{
    using OSDBnet;
    using System;
    using System.Collections.Generic;

    public interface ISubtitleService : IDisposable
    {
        IList<Subtitle> SearchSubtitlesForVideoFile(string filePath);

        void DownloadSubtitle(Subtitle subtitle, string path);

        void EnsureSubtitleIsUTF8(string subtitlePath);
    }
}