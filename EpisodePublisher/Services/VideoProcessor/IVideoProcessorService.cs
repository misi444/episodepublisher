﻿namespace EpisodePublisher.Services.VideoProcessor
{
    public interface IVideoProcessorService
    {
        void BurnSubtitleToVideo(string originalVideoFile, string subtitleFile, string targetFile);
    }
}