﻿namespace EpisodePublisher.Services.VideoProcessor
{
    using System.Diagnostics;

    public class LocalVideoProcessorService : ServiceBase, IVideoProcessorService
    {
        private string ffmpegCommand;

        public LocalVideoProcessorService(string ffmpegCommand) : base()
        {
            this.ffmpegCommand = ffmpegCommand;
        }

        public void BurnSubtitleToVideo(string originalVideoFile, string subtitleFile, string targetFile)
        {
            Log.InfoFormat("Starting video processing at {0}", targetFile);

            Process process = new Process
            {
                StartInfo = new ProcessStartInfo(this.ffmpegCommand, $"-y -loglevel quiet -i \"{originalVideoFile}\" -vf \"subtitles='{subtitleFile}'\" \"{targetFile}\"") { UseShellExecute = false }
            };

            process.Start();
            process.WaitForExit();

            Log.InfoFormat("Video processing complete.");
        }
    }
}