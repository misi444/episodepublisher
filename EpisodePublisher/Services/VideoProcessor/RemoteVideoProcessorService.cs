﻿namespace EpisodePublisher.Services.VideoProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using TcpUdp;

    public class RemoteVideoProcessorService : ServiceBase, IVideoProcessorService
    {
        private IPAddress ip;
        private IPAddress mask;
        private int servicePort;
        private string mac;
        private int wolPort;

        public RemoteVideoProcessorService(IPAddress ip, IPAddress mask, int servicePort, string mac, int wolPort) : base()
        {
            this.ip = ip;
            this.mask = mask;
            this.servicePort = servicePort;
            this.mac = mac;
            this.wolPort = wolPort;
        }

        public void BurnSubtitleToVideo(string originalVideoFile, string subtitleFile, string targetFile)
        {
            string message = string.Join(";", new List<string> { originalVideoFile, subtitleFile, targetFile });
            ITcpUdpService tcpUdpService = new TcpUdpService();
            string response = string.Empty;

            try
            {
                response = tcpUdpService.SendTcpMessage(ip, servicePort, message);
            }
            catch (SocketException)
            {
                tcpUdpService.WakeComputerUp(mac, ip, mask, wolPort);
                Thread.Sleep(1000 * 60 * 3);
                response = tcpUdpService.SendTcpMessage(ip, servicePort, message);
            }

            if (response != "SUCCESS")
            {
                throw new ApplicationException("Failed to convert video on the remote computer.");
            }
        }
    }
}