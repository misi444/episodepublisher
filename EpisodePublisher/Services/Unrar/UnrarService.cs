﻿namespace EpisodePublisher.Services.Unrar
{
    using SharpCompress.Reader;
    using SharpCompress.Reader.Rar;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class UnrarService : ServiceBase, IUnrarService
    {
        public UnrarService() : base()
        {

        }

        public void UnrarMultiArchive(string folderPath)
        {
            List<FileStream> streams = Directory.GetFiles(folderPath, "*.r??", SearchOption.AllDirectories).ToList().Select(x => File.Open(x, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)).ToList();
            FileStream first = streams.Last();
            streams.RemoveAt(streams.Count - 1);
            streams.Insert(0, first);

            using (RarReader rarReader = RarReader.Open(streams))
            {
                rarReader.WriteAllToDirectory(Config.DownloadLocation);
            }

            streams.ForEach(x => x.Close());
        }
    }
}