﻿namespace EpisodePublisher.Services.Unrar
{
    public interface IUnrarService
    {
        void UnrarMultiArchive(string folderPath);
    }
}