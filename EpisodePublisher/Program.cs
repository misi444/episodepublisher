﻿using EpisodePublisher.Services.BitTorrent;
using EpisodePublisher.Services.DirectoryCleaner;
using EpisodePublisher.Services.Email;
using EpisodePublisher.Services.Http;
using EpisodePublisher.RetryLogic;
using EpisodePublisher.Services.Rss;
using EpisodePublisher.Services.Subtitles;
using EpisodePublisher.Services.Unrar;
using EpisodePublisher.Services.VideoProcessor;
using log4net;
using log4net.Config;
using OSDBnet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace EpisodePublisher
{
    public class Program
    {
        private static ILog Log;

        private static readonly IEmailService EmailService = new EmailService(Config.GmailUsername, Config.GmailPassword, Config.Subject, Config.ErrorSubject, Config.Recipients, Config.Developers);


        static Program()
        {
            // enable e-mail sending from Linux using Mono
            ServicePointManager.ServerCertificateValidationCallback = ((object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true);
        }

        public static void Main(string[] args)
        {
            Log = LogManager.GetLogger(typeof(Program));
            XmlConfigurator.Configure();

            try
            {
                Log.InfoFormat("Program starting...");
                string torrentFileName = null;
                string torrentName = null;

                if (Config.UseRss)
                {
                    IRssService rssService = new RssService(Config.FeedUrl);
                    
                    IList<Uri> matches = new List<Uri>();
                    string filter = $"{ Config.SerieTitle.ToLower()} s{Config.Season}e{Config.Episode} 720p hdtv x264-";
                    IRetrier<IList<Uri>> rssRetrier = new Retrier<IList<Uri>>();
                    matches = rssRetrier.TryWithDelay(() => rssService.GetMatchingRssUris(filter), (result) => result != null && result.Any(), TimeSpan.FromMinutes(5), "No matches found.", TimeSpan.FromSeconds(30), "Error loading and searching RSS feed.");

                    Uri uri = matches.First();
                    Log.InfoFormat("Found uri: {0}", uri);

                    torrentFileName = Path.GetFileName(uri.ToString().Split('?').First());
                    torrentName = torrentFileName.Substring(0, torrentFileName.Length - 8);

                    IHttpService httpService = new HttpService();
                    IRetrier httpRetrier = new Retrier();
                    httpRetrier.TryWithDelay(() => httpService.DownloadFile(uri, Path.Combine(Config.DownloadLocation, torrentFileName)), TimeSpan.FromSeconds(30), "Error downloading torrent file.");
                    Log.InfoFormat("Torrent file downloaded to {0}.", Path.Combine(Config.DownloadLocation, torrentFileName));
                }

                ITorrentService torrentService = null;
                if (Config.DownloadTorrent)
                {
                    torrentFileName = Path.GetFileName(Directory.GetFiles(Config.DownloadLocation, "*.torrent").Single());
                    Log.InfoFormat("Starting torrent...");
                    torrentService = new TorrentService(Config.DownloadLocation, Config.TorrentPort);
                    IRetrier torrentRetrier = new Retrier();
                    torrentRetrier.TryWithDelay(() => torrentService.DownloadTorrent(Path.Combine(Config.DownloadLocation, torrentFileName)), TimeSpan.FromSeconds(30), "Error downloading torrent content.");
                    Log.InfoFormat("Torrent successfully downloaded.");
                }

                if (Config.ExtractContent)
                {
                    Log.InfoFormat("Unraring video content...");
                    IUnrarService unrarService = new UnrarService();
                    IRetrier unrarRetrier = new Retrier();
                    unrarRetrier.TryWithDelay(() => unrarService.UnrarMultiArchive(Config.DownloadLocation), TimeSpan.FromSeconds(30), "Error unraring video content.");

                    Log.InfoFormat("Video content extracted.");
                }

                string extractedFilename = Directory.GetFiles(Config.DownloadLocation, "*.mkv").Single();
                Log.InfoFormat("Extracted content found at {0}", extractedFilename);
                string videoFileName = Path.GetFileName(extractedFilename);
                ISubtitleService subtitleService = new SubtitleService(Config.Language);
                string subtitlePath = null;

                if (Config.DownloadSubtitle)
                {
                    Log.InfoFormat("Looking for existing .srt subtitle files in {0}...", Config.DownloadLocation);
                    List<string> existingSubtitles = Directory.GetFiles(Config.DownloadLocation, "*.srt").ToList();
                    if (existingSubtitles.Any())
                    {
                        Log.InfoFormat("Found {0}. Deleting them...", existingSubtitles.Count);
                        existingSubtitles.ForEach(File.Delete);
                        Log.InfoFormat("Successfully deleted {0} subtitles.", existingSubtitles.Count);
                    }
                    else
                    {
                        Log.InfoFormat("No existing .srt files found.");
                    }

                    Log.InfoFormat("Searching for subtitles based on extracted video file {0}...", videoFileName);
                    IRetrier<IList<Subtitle>> subtitleRetrier = new Retrier<IList<Subtitle>>();
                    IList<Subtitle> subtitles = subtitleRetrier.TryWithDelay(() => subtitleService.SearchSubtitlesForVideoFile(Path.Combine(Config.DownloadLocation, videoFileName)), (result) => result != null && result.Any(), TimeSpan.FromMinutes(5), $"No subtitles found for {videoFileName}.", TimeSpan.FromSeconds(5), $"Error searching subtitles for {videoFileName}.");
                    Log.InfoFormat("Subtitle(s) found.");
                    Log.InfoFormat("Trying to download subtitle...");
                    Subtitle subtitle = subtitles.First();
                    subtitleRetrier.TryWithDelay(() => subtitleService.DownloadSubtitle(subtitle, Config.DownloadLocation), TimeSpan.FromSeconds(5), $"Error downloading subtitle for {videoFileName}.");
                    Log.InfoFormat("Subtitle downloaded.");
                    subtitlePath = Directory.GetFiles(Config.DownloadLocation, "*.srt").Single();
                    Log.InfoFormat("Downloaded subtitle found at {0}", subtitlePath);
                    subtitleService.EnsureSubtitleIsUTF8(subtitlePath);
                }

                string finalFileName = Path.Combine(Config.PublishLocation, Path.GetFileName(videoFileName));

                if (Config.BurnSubtitle)
                {
                    if (string.IsNullOrWhiteSpace(subtitlePath))
                    {
                        subtitlePath = Directory.GetFiles(Config.DownloadLocation, "*.srt").Single();
                        Log.InfoFormat("Downloaded subtitle found at {0}", subtitlePath);
                        subtitleService.EnsureSubtitleIsUTF8(subtitlePath); 
                    }

                    if (Config.Windows)
                    {
                        subtitlePath = subtitlePath.Replace("\\", "\\\\").Insert(1, "\\");
                    }

                    IVideoProcessorService videoConverterService;
                    if (Config.BurnSubtitleLocally)
                    {
                        videoConverterService = new LocalVideoProcessorService(Config.FfmpegCommand);
                    }
                    else
                    {
                        videoConverterService = new RemoteVideoProcessorService(Config.TcpIp, Config.TcpMask, Config.TcpPort, Config.WolMac, Config.WolPort);
                    }

                    videoConverterService.BurnSubtitleToVideo(Path.Combine(Config.DownloadLocation, videoFileName), subtitlePath, finalFileName);
                }

                if (Config.SendNotification)
                {
                    Log.InfoFormat("Sending mail notification to {0} recipients...", Config.Recipients.Count);
                    string body = string.Format(Config.Body, Config.SerieTitle, int.Parse(Config.Season).ToString(), int.Parse(Config.Episode).ToString(), $"{Config.PublishUrl}/{Path.GetFileName(finalFileName)}").Replace("\\n", "<br />");
                    EmailService.SendMailNotification(body, Config.SerieTitle, Config.Language);
                    Log.InfoFormat("Notification sent.");
                }

                if (torrentService != null)
                {
                    Log.InfoFormat("Stopping torrent client...");
                    torrentService.Dispose();
                }


                if (Config.CleanDownloadLocation)
                {
                    Log.InfoFormat("Cleaning up {0}...", Config.DownloadLocation);
                    IDirectoryCleanerService directoryCleanerService = new DirectoryCleanerService();
                    directoryCleanerService.CleanDirectory(Config.DownloadLocation);
                    Log.InfoFormat("Cleanup complete."); 
                }

                Log.Info("Finished. Exiting...");
            }

            catch (Exception e)
            {
                Log.ErrorFormat("Exception occured, trying to send error notification...");
                EmailService.SendErrorMailNotification(e);
                Log.ErrorFormat("Error notification successfully sent. Exiting...");
                Environment.Exit(1);
            }
        }
    }
}