﻿namespace EpisodePublisher.RetryLogic
{
    using System;

    public interface IRetrier
    {
        void TryWithDelay(Action action, TimeSpan delayIfError, string errorMessage);
    }

    public interface IRetrier<T> : IRetrier
    {
        T TryWithDelay(Func<T> func, TimeSpan delayIfError, string errorMessage);

        T TryWithDelay(Func<T> func, Func<T, bool> successFunc, TimeSpan delay, string delayMessage, TimeSpan delayIfError, string errorMessage);
    }
}