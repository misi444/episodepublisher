﻿namespace EpisodePublisher.RetryLogic
{
    using log4net;
    using log4net.Config;
    using System;
    using System.Threading;

    public class Retrier : IRetrier
    {
        private static ILog Log;

        public Retrier()
        {
            Log = LogManager.GetLogger(typeof(Program));
            XmlConfigurator.Configure();
        }

        public void TryWithDelay(Action action, TimeSpan delayIfError, string errorMessage)
        {
            bool success = false;

            do
            {
                try
                {
                    action();
                    success = true;
                }
                catch (Exception e)
                {
                    Log.InfoFormat($"{errorMessage} {e.Message}. Trying again in {delayIfError.TotalSeconds} seconds...");
                    Thread.Sleep(delayIfError);
                }
            }
            while (!success);
        }
    }

    public class Retrier<T> : IRetrier<T>
    {
        private static ILog Log;

        public Retrier()
        {
            Log = LogManager.GetLogger(typeof(Program));
            XmlConfigurator.Configure();
        }

        public void TryWithDelay(Action action, TimeSpan delayIfError, string errorMessage)
        {
            bool success = false;

            do
            {
                try
                {
                    action();
                    success = true;
                }
                catch (Exception e)
                {
                    Log.InfoFormat($"{errorMessage} {e.Message}. Trying again in {delayIfError.TotalSeconds} seconds...");
                    Thread.Sleep(delayIfError);
                }
            }
            while (!success);
        }

        public T TryWithDelay(Func<T> func, TimeSpan delayIfError, string errorMessage)
        {
            T result = default(T);
            bool success = false;

            do
            {
                try
                {
                    result = func();
                    success = true;
                }
                catch (Exception e)
                {
                    Log.InfoFormat($"{errorMessage} {e.Message}. Trying again in {delayIfError.TotalSeconds} seconds...");
                    Thread.Sleep(delayIfError);
                }
            }
            while (!success);

            return result;
        }

        public T TryWithDelay(Func<T> func, Func<T, bool> successFunc, TimeSpan delay, string delayMessage, TimeSpan delayIfError, string errorMessage)
        {
            T result = default(T);
            bool success = false;

            do
            {
                try
                {
                    result = func();
                    if (successFunc(result))
                    {
                        success = true;
                    }
                    else
                    {
                        Log.InfoFormat($"{delayMessage} Trying again in {delay.TotalSeconds} seconds...");
                        Thread.Sleep(delay);
                    }
                }
                catch (Exception e)
                {
                    Log.InfoFormat($"{errorMessage} {e.Message}. Trying again in {delayIfError.TotalSeconds} seconds...");
                    Thread.Sleep(delayIfError);
                }
            }
            while (!success);

            return result;
        }
    }
}