﻿namespace VideoConverterService
{
    using System.ServiceProcess;

    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new VideoConverterService()
            };

            ServiceBase.Run(ServicesToRun);
        }
    }
}