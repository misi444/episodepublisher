﻿namespace VideoConverterService
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Security;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading;

    public partial class VideoConverterService : ServiceBase
    {
        private TcpListener tcpListener;
        private TcpClient tcpClient;

        public VideoConverterService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            string message = "D:\\torrent7\\Awkward.S05E20.720p.HDTV.x264-AVS.mkv;D\\:\\\\torrent7\\\\Awkward.S05E20.HDTV.x264-FLEET.srt;\\\\192.168.0.180\\www\\html\\Awkward.S05E20.720p.HDTV.x264-AVS2.mkv";
            List<string> splittedMessage = message.Split(';').ToList();
            string videoFileName = splittedMessage[0];
            string subtitlePath = splittedMessage[1];
            string finalFileName = splittedMessage[2];
            //string ffmpegCommand = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), Config.FfmpegCommand);
            string ffmpegCommand = @"D:\ffmpeg\ffmpeg.exe";

            using (Process ffmpeg = new Process())
            {
                ffmpeg.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                ffmpeg.StartInfo.FileName = ffmpegCommand;
                ffmpeg.StartInfo.UseShellExecute = false;
                ffmpeg.StartInfo.RedirectStandardError = true;
                ffmpeg.StartInfo.RedirectStandardOutput = true;
                ffmpeg.StartInfo.CreateNoWindow = true;
                ffmpeg.StartInfo.Arguments = $"-y -loglevel quiet -i \"{videoFileName}\" -vf \"subtitles='{subtitlePath}'\" \"{finalFileName}\"";

                ffmpeg.Start();
                ffmpeg.WaitForExit();
            }

            //this.tcpListener = new TcpListener(IPAddress.Any, Config.TcpListenPort);
            //this.tcpListener.Start();

            //Thread listeningThread = new Thread(() =>
            //{
            //    while (true)
            //    {
            //        this.tcpClient = tcpListener.AcceptTcpClient();
            //        ThreadPool.QueueUserWorkItem(param =>
            //        {
            //            string message;
            //            byte[] bytes = new byte[1024];
            //            int i;

            //            using (NetworkStream stream = tcpClient.GetStream())
            //            {
            //                i = stream.Read(bytes, 0, bytes.Length);

            //                message = Encoding.ASCII.GetString(bytes, 0, i);

            //                List<string> splittedMessage = message.Split(';').ToList();
            //                if (splittedMessage.Count == 3)
            //                {
            //                    string videoFileName = splittedMessage[0];
            //                    string subtitlePath = splittedMessage[1];
            //                    string finalFileName = splittedMessage[2];

            //                    string ffmpegCommand = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), Config.FfmpegCommand);

            //                    StringBuilder sb = new StringBuilder();
            //                    using (Process process = new Process())
            //                    {
            //                        process.StartInfo = new ProcessStartInfo(ffmpegCommand, $"-y -i \"{videoFileName}\" -vf \"subtitles='{subtitlePath}'\" \"{finalFileName}\"");
            //                        process.StartInfo.CreateNoWindow = true;
            //                        process.StartInfo.ErrorDialog = false;
            //                        process.StartInfo.RedirectStandardError = true;
            //                        process.StartInfo.RedirectStandardInput = true;
            //                        process.StartInfo.RedirectStandardOutput = true;
            //                        process.StartInfo.UseShellExecute = false;
            //                        process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //                        process.Start();

            //                        while (!process.StandardOutput.EndOfStream)
            //                        {
            //                            sb.AppendLine(process.StandardOutput.ReadLine());
            //                        }

            //                        process.WaitForExit();
            //                        string responseString = process.ExitCode == 0 ? "SUCCESS" : "FAIL";
            //                        if (process.ExitCode != 0)
            //                        {
            //                            // responseString = sb.ToString();
            //                            responseString = ffmpegCommand;
            //                        }

            //                        byte[] responseMessage = Encoding.UTF8.GetBytes(responseString);
            //                        stream.Write(responseMessage, 0, responseMessage.Length);
            //                    }
            //                }
            //            }
            //        }, null);
            //    }
            //});

            //listeningThread.IsBackground = true;
            //listeningThread.Start();
        }

        protected override void OnStop()
        {
            if (this.tcpClient != null)
            {
                this.tcpClient.Close();
            }

            if (this.tcpListener != null)
            {
                this.tcpListener.Stop();
            }
        }
    }
}