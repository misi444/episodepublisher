﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;

namespace VideoConverterService
{
    public static class Config
    {
        public static string FfmpegCommand => ConfigurationManager.AppSettings["FfmpegCommand"];

        public static int TcpListenPort => int.Parse(ConfigurationManager.AppSettings["TcpListenPort"]);
    }
}